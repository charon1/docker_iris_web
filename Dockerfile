FROM python:3.8
WORKDIR /app
COPY . .
COPY ./requiement.txt requiement.txt
RUN pip install -r requiement.txt
ENTRYPOINT [ "python","server.py"]